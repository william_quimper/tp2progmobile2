package ca.cegepgarneau.appachat

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar

class AdminActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)

        val toolbar = findViewById<Toolbar>(R.id.tbar)
        setSupportActionBar(toolbar)

        // Ajout d'un titre à la toolbar
        supportActionBar?.title = "Admin"
        // Sous titre
        supportActionBar?.subtitle = "H2023"
        // Bouton retour
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}