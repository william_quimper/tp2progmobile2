package ca.cegepgarneau.appachat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import ca.cegepgarneau.appachat.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val toolbar = findViewById<Toolbar>(R.id.tbar)
        setSupportActionBar(toolbar)

        // Ajout d'un titre à la toolbar
        supportActionBar?.title = "Premiere page"
        // Sous titre
        supportActionBar?.subtitle = "A2023"

        replaceFragment(HomeFragment())

        binding.bottomNavigationView.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.Home -> {
                    replaceFragment(HomeFragment())
                    // Handle the click for the "Home" item
                    // You can add your custom logic here
                    true
                }
                R.id.Page2 -> {
                    replaceFragment(SecondFragment())
                    true
                }
                else -> false
            }
        }

    }
    // Initialisation du menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.mn_admin -> {
                // TODO : go to admin fragment
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }




    private fun replaceFragment(fragment: Fragment) {

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frame_layout, fragment)
        fragmentTransaction.commit()
    }

}